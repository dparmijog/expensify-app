import { createStore, combineReducers } from 'redux'
import expenseListReducer from './../reducers/expenses'
import filtersReducer from './../reducers/filters'


export default () => {
    const store = createStore(
        combineReducers({
            expenseList: expenseListReducer,
            filters: filtersReducer
        }),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )

    return store
}
