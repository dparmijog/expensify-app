import selectExpensesTotal from './expenses-total'
import expenseList from './../mock/expense.list'

test('it should return 0 if no expenses', () => {
    const response = selectExpensesTotal([])
    expect(response).toBe(0)
})

test('it correctly add up a sinlge expense', () => {
    const response = selectExpensesTotal([ expenseList[0]])
    expect(response).toBe(expenseList[0].amount)
})

test('it correctly add all expenses', () => {
    const response = selectExpensesTotal(expenseList)
    expect(response).toBe(5790)
})