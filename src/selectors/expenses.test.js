import selectExpenses from './expenses'
import moment from 'moment'

import expenseList from '../mock/expense.list'

test('should filter by text value', () => {
    const filters = {
        text: 'e',
        startDate: undefined,
        endDate:undefined
    }
    const result = selectExpenses(expenseList, filters)
    expect(result).toEqual([expenseList[1], expenseList[2]])
})

test('should filter by start date', () => {
    const filters = {
        text: '',
        startDate: moment(0),
        endDate:undefined
    }
    const result = selectExpenses(expenseList, filters)
    expect(result).toEqual([expenseList[0], expenseList[2]])
})

test('should filter by end date', () => {
    const filters = {
        text: '',
        startDate: undefined,
        endDate:moment(0)
    }
    const result = selectExpenses(expenseList, filters)
    expect(result).toEqual([expenseList[0], expenseList[1]])
})

test('should sort by date', () => {
    const filters = {
        text: '',
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined
    }
    const result = selectExpenses(expenseList, filters)
    expect(result).toEqual([expenseList[2], expenseList[0], expenseList[1]])
})

test('should sort by amount', () => {
    const filters = {
        text: '',
        sortBy: 'amount',
        startDate: undefined,
        endDate: undefined
    }
    const result = selectExpenses(expenseList, filters)
    expect(result).toEqual([expenseList[2], expenseList[1], expenseList[0]])
})