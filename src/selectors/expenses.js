import moment from 'moment'

export default (expenseList, { text, sortBy, startDate, endDate}) => {
    return expenseList.filter(({createdAt, description}) => {
        const createdAtMoment = moment(createdAt)
        const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment) : true 
        const endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment) : true 
        const textMatch = typeof text === 'string' && description.toLowerCase().includes(text.toLowerCase())
        return startDateMatch && endDateMatch && textMatch
    }).sort((a, b) => {
        if(sortBy === 'amount') {
            return b.amount - a.amount
        } else if (sortBy === 'date') {
            return b.createdAt - a.createdAt
        }
    })
}