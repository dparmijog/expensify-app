import React from 'react'
import ReactDOM from 'react-dom'

const Info = (props) => (
    <div>
        <h1></h1>
        <p>The info is: {props.info} </p>
    </div>
)

const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin && <p> This is private info pls dont share</p>}
            <WrappedComponent {...props}/>
        </div>
    )
}

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {
                props.isAuthenticated ? (
                    <WrappedComponent {...props}/> 
                ) : ( 
                    <p>you need to log in to see this info</p>
                )
            }
        </div>
    )
}

//require Authentication

const AdminInfo = withAdminWarning(Info)
const AuthInfo = requireAuthentication(Info)
requireAuthentication

ReactDOM.render(<AuthInfo isAuthenticated={true} info="there are the details"/>, document.getElementById('app'))