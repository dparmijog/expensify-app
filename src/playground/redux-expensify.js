
store.subscribe(() => {
    const state = store.getState();
    const visibleExpenseList = getVisibleExpenseList(state.expenseList, state.filterList)
    console.log(visibleExpenseList)
})

const expenseOne = store.dispatch(addExpense({
    description: 'rent fee',
    amount: 30,
    createdAt: 1000
}))

const expenseTwo = store.dispatch(addExpense({
    description: 'coffee',
    amount: 300,
    createdAt: 0
}))

// store.dispatch(removeExpense({
//     id: expenseOne.expense.id
// }))

// store.dispatch(editExpense({
//     id: expenseTwo.expense.id,
//     updates: {
//         description: 'hi coffee',
//         amount: 5
//     }
// }))



// store.dispatch(sortByDate())

store.dispatch(sortByAmount())

store.dispatch(setStartDate(0))

store.dispatch(setEndDate(1250))

store.dispatch(setTextFilter('fee'))
