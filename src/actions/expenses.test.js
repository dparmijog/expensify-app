import { addExpense, editExpense, removeExpense } from './expenses'

test('should setup remove expense action object', () => {
    const action = removeExpense({id:'123abc'})
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        expense: {
            id: '123abc'
        }
    })
})


test('should setup remove expense action object', () => {
    const action = editExpense({id:'123abc', updates:{ description: 'asd'}})
    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id: '123abc',
        updates: {
            description: 'asd'
        }
    })
})

test('should setup add expense action object', () => {
    const expenseData = {
        description: 'xD',
        amount: 1200,
        note: '',
        createdAt: 0
    }
    const action = addExpense(expenseData)
    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: {
            id: expect.any(String),
            ...expenseData
        }
    })
})

test('should setup add expense action object with default values', () => {
    const expenseData = {  }
    const action = addExpense(expenseData)
    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: {
            id: expect.any(String),
            description: '',
            note: '',
            amount: 0,
            createdAt: 0
            
        }
    })
})