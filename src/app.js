import React  from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter'

import configureStore from './store/configureStore';
import { addExpense } from './actions/expenses'
import { setTextFilter } from './actions/filters'

import './../node_modules/normalize.css/normalize.css'
import './styles/styles.scss'
import 'react-dates/lib/css/_datepicker.css'
import moment from 'moment'

const store = configureStore();

store.dispatch(addExpense({
    description: 'Water bill',
    amount: 3000,
    createdAt: moment().startOf('month').valueOf(),
}))

store.dispatch(addExpense({
    description: 'Rent bill',
    amount: 109500,
    createdAt: moment().startOf('month').valueOf(),
}))

store.dispatch(addExpense({
    description: 'Gas bill',
    amount: 3400,
    createdAt: moment().startOf('month').valueOf(),
}))

console.log(store.getState())

store.dispatch(setTextFilter(''))


ReactDOM.render(<Provider store={ store }><AppRouter /></Provider>, document.getElementById('app'))