import React from 'react'
import { shallow } from 'enzyme'
import { EditExpense } from './Edit'
import expenseList from '../mock/expense.list'


let editExpense, removeExpense, history,  wrapper

beforeEach(() => {
    editExpense = jest.fn()
    removeExpense = jest.fn()
    history = { push: jest.fn() }
    wrapper = shallow(<EditExpense 
        expense={expenseList[1]}
        editExpense={editExpense}
        removeExpense={removeExpense}
        history={history}
    />)
})

test ('should render EditExpense correctly' , () => {
    expect(wrapper).toMatchSnapshot()
})

test ('should handle editExpense' , () => {
    const newExpense = {
        description: 'asd',
        amount: expenseList[1].amount,
        createdAt: expenseList[1].createdAt,
        note: expenseList[1].note  
    }
    wrapper.find('ExpenseForm').prop('onSubmit')({...newExpense})
    expect(editExpense).toHaveBeenLastCalledWith({
        id: expenseList[1].id,
        updates: newExpense
    })
    expect(history.push).toHaveBeenLastCalledWith('/')
})

test ('should handle removeExpense' , () => {
    wrapper.find('button').simulate('click')
    expect(removeExpense).toHaveBeenLastCalledWith({
        id: expenseList[1].id
    })
    expect(history.push).toHaveBeenLastCalledWith('/')
})