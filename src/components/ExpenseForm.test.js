import React from 'react'
import { shallow } from 'enzyme'
import ExpenseForm from './ExpenseForm'
import expenseList from './../mock/expense.list'
import moment from 'moment'

test('should render ExpenseForm correctly', () => {
    const wrapper = shallow(<ExpenseForm/>)
    expect(wrapper).toMatchSnapshot()
})


test('should render ExpenseForm correctly with expense data', () => {
    const wrapper = shallow(<ExpenseForm expense={expenseList[2]}/>)
    expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseForm correctly with expense data', () => {
    const wrapper = shallow(<ExpenseForm/>)
    wrapper.find('form').simulate('submit', {
        preventDefault: () => { }
    })
    expect(wrapper.state().error.length).toBeGreaterThan(0)
    expect(wrapper).toMatchSnapshot()
})

test('should set description on input change', () => {
    const wrapper = shallow(<ExpenseForm/>)
    const value = 'text'
    wrapper.find('input').at(0).simulate('change', {
        target: {
            value
        }
    })
    expect(wrapper.state().description).toBe(value)
    expect(wrapper).toMatchSnapshot()
})

test('should set note on input change', () => {
    const wrapper = shallow(<ExpenseForm/>)
    const value = 'text'
    wrapper.find('textarea').at(0).simulate('change', {
        target: {
            value
        }
    })
    expect(wrapper.state().note).toBe(value)
    expect(wrapper).toMatchSnapshot()
})

test('should set amount on input change valid', () => {
    const wrapper = shallow(<ExpenseForm/>)
    const value = '12.95'
    wrapper.find('input').at(1).simulate('change', {
        target: {
            value
        }
    })
    expect(wrapper.state().amount).toBe(value)
    expect(wrapper).toMatchSnapshot()
})

test('should set note on input change invalid', () => {
    const wrapper = shallow(<ExpenseForm/>)
    const value = '.9552'
    wrapper.find('input').at(1).simulate('change', {
        target: {
            value
        }
    })
    expect(wrapper.state().amount).toBe('')
    expect(wrapper).toMatchSnapshot()
})

test('should call onSubmit prop for valid form submission', () => {
    const onSubmitSpy = jest.fn()
    const wrapper = shallow(<ExpenseForm expense={expenseList[0]} onSubmit={onSubmitSpy}/>)
    wrapper.find('form').simulate('submit', {
        preventDefault: () => { }
    })
    expect(wrapper.state().error.length).toBe(0)
    expect(onSubmitSpy).toHaveBeenLastCalledWith({
        description:expenseList[0].description,
        amount: expenseList[0].amount,
        note: expenseList[0].note,
        createdAt: expenseList[0].createdAt
    })
})

test('should set new date on date change', () => {
    const now = moment()
    const wrapper = shallow(<ExpenseForm/>)
    wrapper.find('withStyles(SingleDatePicker)').props().onDateChange(now)
    expect(wrapper.state().createdAt).toEqual(now)
})

test('should set focus on date focus', () => {
    const focused = true
    const wrapper = shallow(<ExpenseForm/>)
    wrapper.find('withStyles(SingleDatePicker)').props().onFocusChange({ focused })
    expect(wrapper.state().calendarFocus).toEqual(focused)
})