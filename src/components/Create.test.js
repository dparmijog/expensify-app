import React from 'react'
import { shallow } from 'enzyme'
import { CreateExpense } from './Create'
import expenseList from '../mock/expense.list'


let addExpense, history, wrapper

beforeEach(() => {
    addExpense = jest.fn()
    history = { push: jest.fn() }
    wrapper = shallow(<CreateExpense addExpense={addExpense} history={history}/>)
})

test('should render CreateExpense correctly', () => {
    expect(wrapper).toMatchSnapshot()
})

test('should handle on submit', () => {
    wrapper.find('ExpenseForm').prop('onSubmit')(expenseList[1])
    expect(history.push).toHaveBeenLastCalledWith('/')
    expect(addExpense).toHaveBeenLastCalledWith(expenseList[1])
    //uexpect(wrapper).toMatchSnapshot()
})