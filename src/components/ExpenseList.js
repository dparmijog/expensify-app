import React  from 'react'
import ExpenseItem from './ExpenseItem'
import { connect } from 'react-redux'

import selectExpenses from './../selectors/expenses'

export const ExpenseList = ({expenseList}) => (
    <div>
        {
            expenseList.length === 0 ? (
                <p>No Expenses</p>
            ) : (
                expenseList.map(expense => <ExpenseItem key={expense.id} {...expense}/>)
            )
        }
    </div>
)

export default connect(({ expenseList, filters }) => ({ 
    expenseList: selectExpenses(expenseList, filters) 
}))(ExpenseList);
