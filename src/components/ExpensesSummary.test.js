import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseSummary } from './ExpenseSummary'

test('should correctly render ExpenseSummary with 1 expense',() => {
    const wrapper = shallow(<ExpenseSummary count={1} total={254}/>)
    expect(wrapper).toMatchSnapshot()
})

test('should correctly render ExpenseSummary with multiple expense',() => {
    const wrapper = shallow(<ExpenseSummary count={23} total={212312354}/>)
    expect(wrapper).toMatchSnapshot()
})