import React from 'react'
import ExpenseForm from './ExpenseForm'
import { connect } from 'react-redux'
import { addExpense } from './../actions/expenses'

export class CreateExpense extends React.Component {
    onSubmit = (expense) => {
        this.props.addExpense(expense)
        this.props.history.push('/')
    }
    render() {
        return (
            <div>
                <h1>Add Expense</h1>
                <ExpenseForm
                    onSubmit={this.onSubmit}
                />
            </div>
        )
    }
}


export default connect(undefined, (dispatch) => {
    return {
        addExpense: (expense) => {
            return dispatch(addExpense(expense))
        }
    }
})(CreateExpense)