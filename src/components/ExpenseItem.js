import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import numeral from 'numeraljs'


const ExpenseItem = ({id, description, amount, createdAt}) => (
    <div>
        <Link to={`/edit/${id}`}><h3>{description}</h3></Link>
        <p>
        <span>{numeral(amount).format('$0,0.00')} </span>
        - 
        <span> {moment(createdAt).format('MMMM Do, YYYY')}</span></p>
    </div>
)

export default ExpenseItem;