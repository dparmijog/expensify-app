import React  from 'react'

import ExpenseList from './ExpenseList'
import ExpenseFilters from './ExpenseFilters'
import ExpenseSummary from './ExpenseSummary'

export default  () => (
    <div>
        Dashboard component
        <ExpenseFilters/>
        <ExpenseSummary/>
        <ExpenseList/>
    </div>
)