import React from 'react'

import { connect } from 'react-redux'

import { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate } from './../actions/filters'
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates'
//import 'react-dates/lib/css/_datepicker.css'

export class ExpenseFilters extends React.Component {
    state = {
        calendarFocused: null
    }
    onDatesChange = ({startDate, endDate}) => {
        this.props.setStartDate(startDate)
        this.props.setEndDate(endDate)
    }
    onFocusChange = (calendarFocused) => {
        this.setState(() => ({
            calendarFocused
        }))
    }

    onTextChange = (e) => {
        this.props.setTextFilter(e.target.value)
    }

    onSortChange = (e) => {
        if(e.target.value === 'date') {
            this.props.sortByDate()
        } else if(e.target.value === 'amount'){
            this.props.sortByAmount()
        }
    }
    render() {
        return (
            <div>
                <input type="text" value={this.props.filters.text} onChange={this.onTextChange}/>
                <select value={this.props.filters.sortBy } onChange={this.onSortChange}>
                    <option value="date">date</option>
                    <option value="amount" >amount</option>
                    
                </select>
                <DateRangePicker 
                        startDateId={'unique-start-date'}
                        startDate={this.props.filters.startDate}
                        endDateId={'unique-end-date'}
                        endDate={this.props.filters.endDate}
                        onDatesChange={this.onDatesChange}
                        focusedInput={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        isOutsideRange={() => false}
                    />
            </div>
        )
    }
}

export default connect(
    ({ filters }) => ({ filters }),
    (dispatch) => ({
        setTextFilter: (text) => dispatch(setTextFilter(text)),
        sortByDate: () => dispatch(sortByDate),
        sortByAmount: () => dispatch(sortByAmount),
        setStartDate: (startDate) => dispatch(setStartDate(startDate)),
        setEndDate: (endDate) => dispatch(setEndDate(endDate))
    })
)(ExpenseFilters)