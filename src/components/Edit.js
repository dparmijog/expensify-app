import React from 'react'
import ExpenseForm from './ExpenseForm'
import { editExpense, removeExpense } from './../actions/expenses'
import { connect } from 'react-redux'

export class EditExpense extends React.Component {
    onSubmit = (expense) => {
        this.props.editExpense({
            id: this.props.expense.id,
            updates: expense
        })
        this.props.history.push('/')
    }

    onClick = () => {
        this.props.removeExpense({ id: this.props.expense.id })
        this.props.history.push('/')
    }

    render() {
        return (
            <div>
                Edit component {this.props.expense.id}
                <ExpenseForm
                    expense={this.props.expense}
                    onSubmit={this.onSubmit}
                />
                <button onClick={this.onClick}>Remove</button>
            </div>
        )
    }
}


export default connect(
    (state, props) => ({ 
        expense: state.expenseList.find((expense) => expense.id === props.match.params.id) 
    }),
    (dispatch) => ({
        editExpense: ({ id, updates }) => dispatch(editExpense({ id, updates })),
        removeExpense: ({ id }) => dispatch(removeExpense({ id }))
    })
)(EditExpense)