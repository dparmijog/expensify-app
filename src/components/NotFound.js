import React  from 'react'
import { Link } from 'react-router-dom'

export default () => (
    <div>
        404 Not Found<br/>
        <Link to="/">Go Home</Link>

    </div>
)