import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseList } from './ExpenseList'
import expenseList from './../mock/expense.list'
test('should render ExpenseList with expenses',() => {
    const wrapper = shallow(<ExpenseList expenseList={expenseList}/>)
    expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseList with with empty message',() => {
    const wrapper = shallow(<ExpenseList expenseList={[]}/>)
    expect(wrapper).toMatchSnapshot()
})