import React from 'react'
import { shallow } from 'enzyme'
import NotFound from './NotFound'
import expenseList from './../mock/expense.list'

test('should render NotFound correctly',() => {
    const wrapper = shallow(<NotFound/>)
    expect(wrapper).toMatchSnapshot()
})