import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseFilters } from './ExpenseFilters'
import filters from '../mock/filters'
import moment from 'moment'


let wrapper, setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate

beforeEach(() => {
    setTextFilter = jest.fn()
    sortByDate = jest.fn()
    sortByAmount = jest.fn()
    setStartDate = jest.fn()
    setEndDate = jest.fn()

    wrapper = shallow(<ExpenseFilters
            filters={filters[0]}
            setTextFilter={setTextFilter}
            sortByDate={sortByDate}
            sortByAmount={sortByAmount}
            setStartDate={setStartDate}
            setEndDate={setEndDate}
        />)
})

test('should render ExpenseFilters correctly', () => {
    expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseFilters with all date correctly', () => {
    wrapper.setProps({ filters: filters[1] })
    expect(wrapper).toMatchSnapshot()
})

test('should handle text change', () => {
    const value = 'rent'
    wrapper.setProps({ filters: filters[1] })
    wrapper.find('input').simulate('change', {
        target: { value }
    })
    expect(setTextFilter).toHaveBeenLastCalledWith(value)
    
})

test('should sort by date', () => {
    const value = 'date'
    wrapper.setProps({ filters: filters[1] })
    wrapper.find('select').simulate('change', {
        target: { value }
    })
    expect(sortByDate).toHaveBeenCalled()
})

test('should sort by amount', () => {
    const value = 'amount'
    wrapper.find('select').simulate('change', {
        target: { value }
    })
    expect(sortByAmount).toHaveBeenCalled()
})

test('should handle date changes', () => {
    wrapper.setProps({ filters: filters[1]})
    const startDate = moment(0).add(4, 'years')
    const endDate = moment(0).add(8, 'years')
    wrapper.find('withStyles(DateRangePicker)').prop('onDatesChange')({
        startDate,
        endDate
    })
    expect(setStartDate).toHaveBeenLastCalledWith(startDate)
    expect(setEndDate).toHaveBeenLastCalledWith(endDate)
})

test('should handle calendar focus', () => {
    wrapper.setProps({ filters: filters[1] })
    const calendarFocused = 'endDate'
    wrapper.find('withStyles(DateRangePicker)').prop('onFocusChange')( calendarFocused )
    expect(wrapper.state('calendarFocused')).toBe(calendarFocused)
})