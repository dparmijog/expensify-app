import React from 'react'
import { shallow } from 'enzyme'
import ExpenseItem from './ExpenseItem'
import expenseList from './../mock/expense.list'

test('should render ExpenseItem with an expenses',() => {
    const wrapper = shallow(<ExpenseItem {...expenseList[0]}/>)
    expect(wrapper).toMatchSnapshot()
})
