import React from 'react'
import moment from 'moment'
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates'


class ExpenseForm extends React.Component {
    constructor(props) {
        super(props)
        const {description = '', amount = '', note = '', createdAt = moment()} = props.expense ? props.expense : {}
        this.state = {
            description: description ,
            amount: amount ? (amount / 100).toString() : '',
            note: note ,
            createdAt: createdAt ? moment(createdAt) : moment(),
            calendarFocus: false,
            error: ''
        }
    }
    

    onFormSubmit = (e) => {
        e.preventDefault()
        const { description, amount, note, createdAt } = this.state
        if(!this.state.description || !this.state.amount) {
            this.setState(() => ({ error: 'provide description and amount'}))
        } else {
            this.setState(() => ({ error: ''}))
            this.props.onSubmit({
                description,
                amount: parseFloat(amount, 10) * 100,
                note,
                createdAt: createdAt.valueOf()
            })
        }
        

        // this.setState(() => ({
        //     description: '',
        //     amount: '',
        //     note: '',
        // }))
    }

    onDescriptionChange = (e) => {
        const description = e.target.value
        this.setState(() => ({
            description
        }))
    }

    onAmountChange = (e) => {
        const amount = e.target.value
        if(!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({
                amount
            }))
        }
        
    }

    onDateChange = (createdAt) => {
        if(createdAt) {
            this.setState(() => ({ createdAt }))
        }
        
    }
    onFocusChange = ( {focused} ) => {
        this.setState(() => ({
            calendarFocus: focused
        }))
    }

    onNoteChange = (e) => {
        const note = e.target.value
        this.setState(() => ({
            note
        }))
    }

    render() {
        return (
            <div>
            <p>{this.state.error}</p>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="description" autoFocus value={this.state.description} onChange={this.onDescriptionChange}/>
                    <input type="text" placeholder="amount" value={this.state.amount} onChange={this.onAmountChange}/>
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.calendarFocus}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                    <textarea placeholder="add a note for your expense" onChange={this.onNoteChange}/>
                    
                    <button>Add Expense</button>
                </form>
            </div>
        )
    }
}

export default ExpenseForm