import React from 'react'
import selectExpensesTotal from '../selectors/expenses-total'
import selectExpenses from '../selectors/expenses'
import { connect } from 'react-redux';
import numeral from 'numeraljs'

export const ExpenseSummary = ({count, total}) => (
    <div>
        <p>Viewing {count} from {total === 1 ? 'expense' : 'expenses'} totalling {numeral(total).format('$0,0.00')}</p>
    </div>
)

export default connect(({expenseList, filters}) => {
    const filteredExpenses = selectExpenses(expenseList, filters) 
    console.log(filteredExpenses)
    return {
        count: filteredExpenses.length,
        total: selectExpensesTotal(filteredExpenses)
    }
})(ExpenseSummary)