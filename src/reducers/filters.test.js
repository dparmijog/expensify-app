import filtersReducer from './filters'
import moment from 'moment'

test('should setup default filter value', () => {
    const state = filtersReducer(undefined, { type: '@@INIT' })

    expect(state).toEqual({
        text: '',
        sortBy: 'date',
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month')
    })
})


test('should setup sort by amount filter value', () => {
    const state = filtersReducer(undefined, { type: 'SORT_BY_AMOUNT' })

    expect(state.sortBy).toEqual('amount')
})

test('should setup sort by date filter value', () => {
    const currentState = {
        text: '',
        sortBy: 'amount',
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month')
    }
    const state = filtersReducer(currentState, { type: 'SORT_BY_DATE' })

    expect(state.sortBy).toEqual('date')
})

test('should setup text filter value', () => {

    const state = filtersReducer(undefined, { type: 'SET_TEXT_FILTER', text: 'a' })

    expect(state.text).toEqual('a')
})

test('should setup start date filter value', () => {
    
    const date = moment().add(4, 'days')
    const state = filtersReducer(undefined, { type: 'SET_START_DATE', startDate: date })

    expect(state.startDate).toEqual(date)
})

test('should setup start date filter value', () => {

    const date = moment().add(4, 'days')
    const state = filtersReducer(undefined, { type: 'SET_END_DATE', endDate: date })

    expect(state.endDate).toEqual(date)
})