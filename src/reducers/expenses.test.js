import expenseListReducer from './expenses'
import expenseList from '../mock/expense.list'

test('should setup default expenses value', () => {
    const state = expenseListReducer(undefined, { type: '@@INIT' })
    expect(state).toEqual([])
})

test('should setup add expenses value', () => {
    const expense = {
        description: 'a',
        amount: 100
    }
    const state = expenseListReducer(expenseList, { type: 'ADD_EXPENSE', expense })
    expect(state).toEqual([ ...expenseList, expense ])
})

test('should setup edit expenses value', () => {
    const id = expenseList[0].id
    
    const updates = {
        description: 'b',
        amount: 300
    }
    const state = expenseListReducer(expenseList, { type: 'EDIT_EXPENSE', id, updates })
    expect(state).toEqual([{
        ...expenseList[0],
        ...updates
    }, expenseList[1], expenseList[2]])
})

test('should not edit expenses value', () => {
    const id = 'asdqw'
    
    const updates = {
        description: 'b',
        amount: 300
    }
    const state = expenseListReducer(expenseList, { type: 'EDIT_EXPENSE', id, updates })
    expect(state).toEqual(expenseList)
})

test('should setup remove expenses value', () => {
    const expense = {
        id: expenseList[1].id
    }
    const state = expenseListReducer(expenseList, { type: 'REMOVE_EXPENSE', expense })
    expect(state).toEqual([expenseList[0], expenseList[2]])
})

test('should not remove expenses if not found', () => {
    const expense = {
        id: 'asb'
    }
    const state = expenseListReducer(expenseList, { type: 'REMOVE_EXPENSE', expense })
    expect(state).toEqual(expenseList)
})